from django.urls import path
from site_spbfkk.views import *

from site_spbfkk.views.events import ShowEvent, add_event_results, delete_event_result

app_name = 'site_spbfkk'
urlpatterns = [
    path('', SportsmenAll.as_view(), name='home'),
    path('show_rating_sportsmen/', SportsmenAllRating.as_view(), name='show_rating_sportsmen'),
    path('show_rating_coaches/', CoachesRatings.as_view(), name='show_rating_coaches'),
    path('coaches/', CoachesAll.as_view(), name='coaches'),
    path('addsportsmen/', add_sportsmen, name='addsportsmen'),
    path('sportsmen/<int:pk>/edit/', edit_sportsmen_data, name='edit_sportsmen_data'),
    path('sportsmen/<int:pk>/add_data/', add_sportsmen_data, name='add_sportsmen_data'),
    path('sportsmen/<int:pk>/delete/', delete_sportsmen, name='delete_sportsmen'),
    path('sportsmen/<int:pk>/', ShowDataSportsmen.as_view(), name='showsportsmen'),
    path('sportsmen/<int:pk>/documents/', sportsmen_documents, name='sportsmen_documents'),
    path('sportsmen/<int:pk>/documents/del', delete_sportsmen_documents, name='delete_sportsmen_documents'),
    path('sportsmen/<int:pk>/documents/edit/', edit_sportsmen_document, name='edit_sportsmen_document'),
    path('sports_hall/', SportsHallAll.as_view(), name='sports_hall'),
    path('sports_hall/<int:pk>/', ShowSportsHall.as_view(), name='show_sports_hall'),
    path('sports_hall/<int:pk>/add_schedule', add_schedule, name='add_schedule'),
    path('sports_hall/add_sports_hall/', add_sports_hall, name='add_sports_hall'),
    path('coaches/<int:pk>/', ShowDataCoach.as_view(), name='show_coach'),
    path('coaches/<int:pk>/add_data/', add_coach_data, name='add_coach_data'),
    path('coaches/<int:pk>/edit/', edit_coach_data, name='edit_coach_data'),
    path('coaches/<int:pk>/documents/', trainers_documents, name='trainers_documents'),
    path('coaches/<int:pk>/documents/del/', delete_trainers_documents, name='delete_trainers_documents'),
    path('coaches/<int:pk>/documents/edit/', edit_coach_document, name='edit_coach_document'),
    path('events/add_event/', add_event, name='add_event'),
    path('events/', AllEvents.as_view(), name='all_events'),
    path('events/<int:pk>/', ShowEvent.as_view(), name='show_event'),
    path('events/<int:pk>/edit_event/', edit_event_data, name='edit_event'),
    path('events/<int:pk>/documents/', add_event_documents, name='add_event_documents'),
    path('events/<int:pk>/documents/del/', delete_event_documents, name='del_event_documents'),
    path('events/<int:pk>/documents/edit/', edit_event_document, name='edit_event_document'),
    path('events/<int:pk>/add_results/', add_event_results, name='add_event_results'),
    path('events/<int:pk>/add_results/del/', delete_event_result, name='delete_event_result'),
    path('events/<int:pk>/add_applications/', add_event_applications, name='add_event_applications'),
    path('events/<int:pk>/add_applications/del/', delete_event_applications, name='delete_event_applications'),
    path('export_excel', export_excel, name='export_excel'),

]

