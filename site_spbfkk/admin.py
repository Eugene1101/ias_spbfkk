from django.contrib import admin

from site_spbfkk.models import SportsClub, Coach

class CoachAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'patronymic', 'club')
    list_display_links = ('last_name', 'first_name', 'patronymic')

class SportsClubAdmin(admin.ModelAdmin):
    list_display = ('club_name', 'head_of_the_club')

admin.site.register(SportsClub, SportsClubAdmin)
admin.site.register(Coach, CoachAdmin)



