from django.contrib.auth.models import User
from django.db import models

from django.urls import reverse
from django.utils import timezone

SPORTS_CATEGORY = [
        ('3J', '3 юношеский разряд'),
        ('2J', '2 юношеский разряд'),
        ('1J', '1 юношеский разряд'),
        ('3C', '3 разряд'),
        ('2C', '2 разряд'),
        ('1C', '1 разярд'),
        ('KMS', 'КМС'),
        ('MS', 'МС'),
        ('MSIC', 'МСМК'),
        ('HMS', 'ЗМС'),
        ('NN', 'Не выбрано'),
    ]

DEGREE = [
        ('KU', 'Кю'),
        ('DAN', 'Дан'),
    ]

DEGREE_LEVEL = [
        ('O', '1'),
        ('TW', '2'),
        ('T', '3'),
        ('F', '4'),
        ('Fv', '5'),
        ('S', '6'),
        ('Sn', '7'),
        ('E', '8'),
        ('N', '9'),
        ('TE', '10'),
        ('En', '11'),
        ('Z', '0'),
        ('NN', 'Не выбрано'),
    ]

JUDGING_CATEGORY = [
        ('3JC', '3'),
        ('2JC', '2'),
        ('1JC', '1'),
        ('JRC', 'СВК'),
        ('N', 'Отсутствует'),
    ]

class SportsClub(models.Model):
    club_name = models.CharField(max_length=30, verbose_name='Название клуба')
    year_of_foundation = models.DateField(verbose_name='Дата основания')
    head_of_the_club = models.ForeignKey('Coach',
                                         on_delete=models.SET_NULL,
                                         blank=True, null=True,
                                         verbose_name='Руководитель',
                                         )
    date_added = models.DateField(verbose_name='Дата добавления', default=timezone.now)
    archive = models.BooleanField(default=False, verbose_name='Архив')

    class Meta:
        verbose_name = 'Спортивный клуб'
        verbose_name_plural = 'Спортивные клубы'
        ordering = ['club_name']

    def __str__(self):
        return self.club_name

    def get_absolut_url(self):
        return f'/sportsclub/{self.pk}/'


class Coach(models.Model):
    club = models.ForeignKey(SportsClub,
                                  on_delete=models.SET_NULL,
                                  blank=True, null=True,
                                  verbose_name='Клуб',
                                  )
    coach = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True)
    last_name = models.CharField(max_length=20, verbose_name='Фамилия')
    first_name = models.CharField(max_length=15, verbose_name='Имя')
    patronymic = models.CharField(max_length=20, verbose_name='Отчество')
    date_added = models.DateField(verbose_name='Дата добавления', default=timezone.now)


    class Meta:
        verbose_name = 'Тренер'
        verbose_name_plural = 'Тренеры'
        ordering = ['club', 'last_name', 'first_name', 'patronymic']
        app_label = 'site_spbfkk'

    def __str__(self):
       # if self.User:
#            return self.User.laste_name + ' ' + self.User.first_name

        full_name = self.last_name + ' ' + self.first_name + ' ' + self.patronymic
        return full_name

    def get_absolut_url(self):
        return f'/coaches/{self.pk}/'

    # def get_absolut_url(self):
    #     return reverse('coaches', kwargs={'coach_id': self.pk})


class CoachDate(models.Model):
    coach = models.ForeignKey(Coach,
                              on_delete=models.CASCADE,
                              verbose_name='Тренер',
                              )
    year_of_birth = models.DateField(verbose_name='Дата рождения')
    coaching_title = models.TextField(verbose_name='Звания')
    sports_category = models.CharField(choices=SPORTS_CATEGORY,
                                       default='NN',
                                       verbose_name='Спортивный разряд',
                                       max_length=18,
                                       )
    degree = models.CharField(choices=DEGREE,
                              default='KU',
                              verbose_name='Степень',
                              max_length=3,
                              )
    degree_level = models.CharField(choices=DEGREE_LEVEL,
                                    default='Z',
                                    verbose_name='Градация степени',
                                    max_length=10,
                                    )
    judging_category = models.CharField(choices=JUDGING_CATEGORY,
                                        default='N',
                                        verbose_name='Судейская категория',
                                        max_length=11,
                                        )
    sports_results = models.TextField(verbose_name='Спортивные результаты', default=None)
    coach_rating = models.PositiveIntegerField(verbose_name='Рейтинг тренера', default=0)
    education = models.TextField(verbose_name='Образование')
    phone_number = models.CharField(max_length=11, verbose_name='Номер телефона')
    e_mail = models.EmailField(verbose_name='Адрес электронной почты')
    archive = models.BooleanField(default=False, verbose_name='Архив')



class SportsHall(models.Model):
    sports_club = models.ForeignKey(SportsClub,
                                    on_delete=models.SET_NULL,
                                    blank=True, null=True,
                                    verbose_name='Спортиный клуб')
    coach = models.ForeignKey(Coach,
                              on_delete=models.SET_NULL,
                              blank=True, null=True,
                              verbose_name='Тренер',
                              )
    region = models.CharField(max_length=30, verbose_name='Регион')
    locality = models.CharField(max_length=20, verbose_name='Населённый пункт')
    district = models.CharField(max_length=20, verbose_name='Район')
    street = models.CharField(max_length=20, verbose_name='Улица')
    house_number = models.CharField(max_length=5, verbose_name='Номер дома')

    def get_absolut_url(self):
        return f'/sports_hall/{self.pk}/'

    # def __str__(self):
    #     return self.

class TrainingSchedule(models.Model):
    WEEK = [
        ('MD', 'Понедельник'),
        ('TS', 'Вторник'),
        ('WD', 'Среда'),
        ('TH', 'Четверг'),
        ('FD', 'Пятница'),
        ('SD', 'Суббота'),
        ('SND', 'Воскресенье'),
        ('NN', 'Не выбрано')
    ]
    address_hall = models.ForeignKey(SportsHall,
                                     on_delete=models.CASCADE,
                                     verbose_name='Адрес зала'
                                     )
    day_week = models.CharField(choices=WEEK,
                                default='NN',
                                verbose_name='День недели',
                                max_length=11,
                                )
    training_time = models.CharField(max_length=11, verbose_name='Время')
    category_students = models.CharField(max_length=30, verbose_name='Категория занимающихся')

class TrainersDocuments(models.Model):
    coach = models.ForeignKey(Coach,
                              on_delete=models.CASCADE,
                              verbose_name='Тренер',
                              )
    name_document = models.CharField(max_length=30, verbose_name='Название документа')
    date_issue = models.DateField(verbose_name='Дата выдачи')
    validity_period = models.DateField(verbose_name='Действителен до') # значение по умолчанию?
    number_document = models.CharField(max_length=15, verbose_name='Номер документа')
    issued_by_whom = models.CharField(max_length=30, verbose_name='Кем выдан')
    scanned_copy = models.FileField(verbose_name='Скан-копия', blank=True, upload_to='coaches/%Y-%m-%d')
    date_added = models.DateField(verbose_name='Дата добавления', default=timezone.now, blank=True)

    def get_absolut_url(self):
        return f'/documents/{self.pk}/'



class Finance(models.Model):
    TYPE_FINANCE = [
        ('T', 'Налог'),
        ('IFE', 'Инструкторский взнос'),
        ('AF', 'Годовой взнос'),
        ('NN', 'Не выбрано'),
    ]

    coach = models.ForeignKey(Coach,
                              on_delete=models.CASCADE,
                              verbose_name='Тренер'
                              )
    payment_date = models.DateField(verbose_name='Дата взноса')
    type_finance = models.CharField(choices=TYPE_FINANCE,
                                    default='NN',
                                    verbose_name='Вид взноса',
                                    max_length=20,
                                    )
    contrib_amount = models.PositiveSmallIntegerField(verbose_name='Сумма взноса')

class Sportsmen(models.Model):
    class SexChoices(models.TextChoices):
        MALE = 'Мужской'
        FEMALE = 'Женский'

    club = models.ForeignKey(SportsClub,
                                  on_delete=models.SET_NULL,
                                  blank=True, null=True,
                                  verbose_name='Клуб',
                                  )
    coach = models.ForeignKey(Coach,
                              on_delete=models.SET_NULL,
                              blank=True, null=True,
                              verbose_name='Тренер',
                              )
    last_name = models.CharField(max_length=20, verbose_name='Фамилия')
    first_name = models.CharField(max_length=15, verbose_name='Имя')
    patronymic = models.CharField(max_length=20, verbose_name='Отчество')
    date_added = models.DateField(default=timezone.now, verbose_name='Дата добваления')
    sex = models.CharField(choices=SexChoices.choices, verbose_name='Пол', max_length=20, default=SexChoices.MALE)

    def __str__(self):
        full_name = self.last_name + ' ' + self.first_name + ' ' + self.patronymic
        return full_name

    def get_absolut_url(self):
        return f'/sportsmen/{self.pk}/'

class SportsmenDate(models.Model):
    WEIGHT_CATEGORY_MALE = [
        ('825', '8-9 лет до 25 кг'),
        ('830', '8-9 лет до 30 кг'),
        ('835', '8-9 лет до 35 кг'),
        ('840', '8-9 лет до 40 кг'),
        ('845', '8-9 лет свыше 40 кг'),
        ('1030', '10-11 лет до 30 кг'),
        ('1035', '10-11 лет до 35 кг'),
        ('1040', '10-11 лет до 40 кг'),
        ('1045', '10-11 лет до 45 кг'),
        ('1050', '10-11 лет до 50 кг'),
        ('1055', '10-11 лет до 55 кг'),
        ('1060', '10-11 лет свыше 55 кг'),
        ('1235', '12-13 лет до 35 кг'),
        ('1240', '12-13 лет до 40 кг'),
        ('1245', '12-13 лет до 45 кг'),
        ('1250', '12-13 лет до 50 кг'),
        ('1255', '12-13 лет до 55 кг'),
        ('1260', '12-13 лет до 60 кг'),
        ('1265', '12-13 лет свыше 60 кг'),
        ('1440', '14-15 лет до 40 кг'),
        ('1445', '14-15 лет до 45 кг'),
        ('1450', '14-15 лет до 50 кг'),
        ('1455', '14-15 лет до 55 кг'),
        ('1460', '14-15 лет до 60 кг'),
        ('1465', '14-15 лет до 65 кг'),
        ('1470', '14-15 лет до 70 кг'),
        ('1475', '14-15 лет свыше 70 кг'),
        ('1655', '16-17 лет до 55 кг'),
        ('1660', '16-17 лет до 60 кг'),
        ('1665', '16-17 лет до 65 кг'),
        ('1670', '16-17 лет до 70 кг'),
        ('1675', '16-17 лет до 75 кг'),
        ('1680', '16-17 лет до 80 кг'),
        ('1685', '16-17 лет свыше 80 кг'),
        ('1870', 'мужчины до 70 кг'),
        ('1880', 'мужчины до 80 кг'),
        ('1890', 'мужчины до 90 кг'),
        ('1895', 'мужчины свыше 90 кг'),
        ('KATA', 'Ката'),
        ('KATAG', 'Ката-группа'),
    ]

    WEIGHT_CATEGORY_FEMALE = [
        ('825', '8-9 лет до 25 кг'),
        ('830', '8-9 лет до 30 кг'),
        ('835', '8-9 лет до 35 кг'),
        ('840', '8-9 лет до 40 кг'),
        ('845', '8-9 лет свыше 40 кг'),
        ('1030', '10-11 лет до 30 кг'),
        ('1035', '10-11 лет до 35 кг'),
        ('1040', '10-11 лет до 40 кг'),
        ('1045', '10-11 лет до 45 кг'),
        ('1050', '10-11 лет до 50 кг'),
        ('1055', '10-11 лет до 55 кг'),
        ('1060', '10-11 лет свыше 55 кг'),
        ('1240', '12-13 лет до 40 кг'),
        ('1245', '12-13 лет до 45 кг'),
        ('1250', '12-13 лет до 50 кг'),
        ('1255', '12-13 лет до 55 кг'),
        ('1260', '12-13 лет свыше 55 кг'),
        ('1445', '14-15 лет до 45 кг'),
        ('1450', '14-15 лет до 50 кг'),
        ('1455', '14-15 лет до 55 кг'),
        ('1460', '14-15 лет свыше 55 кг'),
        ('1650', '16-17 лет до 50 кг'),
        ('1655', '16-17 лет до 55 кг'),
        ('1660', '16-17 лет свыше 55 кг'),
        ('1870', 'женщины до 55 кг'),
        ('1880', 'женщины до 65 кг'),
        ('1890', 'женщины свыше 65 кг'),
        ('KATA', 'Ката'),
        ('KATAG', 'Ката-группа'),
    ]
    sportsmen = models.ForeignKey(Sportsmen,
                                  on_delete=models.CASCADE,
                                  verbose_name='Спортсмен',
                                  related_name='sportsmensdata')

    year_of_birth = models.DateField(verbose_name='Дата рождения')
    sportsmen_title = models.TextField(verbose_name='Звания')
    sports_category = models.CharField(choices=SPORTS_CATEGORY,
                                       default='NN',
                                       verbose_name='Спортивный разряд',
                                       max_length=18,
                                       )
    degree = models.CharField(choices=DEGREE,
                              default='KU',
                              verbose_name='Степень',
                              max_length=3,
                              )
    degree_level = models.CharField(choices=DEGREE_LEVEL,
                                    default='Z',
                                    verbose_name='Градация степени',
                                    max_length=10,
                                    )
    judging_category = models.CharField(choices=JUDGING_CATEGORY,
                                        default='N',
                                        verbose_name='Судейская категория',
                                        max_length=11,
                                        )
    sports_results = models.TextField(verbose_name='Спортивные результаты')
    sports_rating = models.PositiveIntegerField(verbose_name='Спортивный рейтинг', default=0)
    weight_category_male = models.CharField(max_length=25,
                                            choices=WEIGHT_CATEGORY_MALE,
                                            verbose_name='Весовая категория', blank=True)
    weight_category_female = models.CharField(max_length=25,
                                            choices=WEIGHT_CATEGORY_MALE,
                                            verbose_name='Весовая категория', blank=True)
    annual_fee = models.BooleanField(default=False, verbose_name='Годовой взнос')
    annual_fee_date = models.DateField(verbose_name='Дата годового взноса', default=timezone.now)
    phone_number = models.CharField(max_length=11, verbose_name='Номер телефона')
    e_mail = models.EmailField(verbose_name='Адрес электронной почты')
    flp_parents_1 = models.CharField(max_length=30, verbose_name='ФИО родителя')
    phone_number_par_1 = models.CharField(max_length=11, verbose_name='Номер телефона родителя')
    flp_parents_2 = models.CharField(max_length=30, verbose_name='ФИО родителя', blank=True)
    phone_number_par_2 = models.CharField(max_length=11, verbose_name='Номер телефона родителя', blank=True)
    archive = models.BooleanField(default=False, verbose_name='Архив', blank=False)


    def __str__(self):
        return self.sportsmen.last_name + ' ' + self.sportsmen.first_name + ' ' + self.sportsmen.patronymic

    def get_absolut_url(self):
        return f'sportsmen/{self.pk}/'

class DocumentsSportsmen(models.Model):
    sportsmen = models.ForeignKey(Sportsmen,
                                  on_delete=models.CASCADE,
                                  verbose_name='Спортсмен'
                                  )
    name_document = models.CharField(max_length=30, verbose_name='Название документа')
    date_issue = models.DateField(verbose_name='Дата выдачи')
    validity_period = models.DateField(verbose_name='Действителен до')  # значение по умолчанию?
    number_document = models.CharField(max_length=15, verbose_name='Номер документа')
    issued_by_whom = models.CharField(max_length=30, verbose_name='Кем выдан')
    date_added = models.DateField(default=timezone.now, verbose_name='Дата добавления')
    scanned_copy = models.FileField(verbose_name='Скан-копия', blank=True, upload_to='sportsmen/%Y-%m-%d')

class Events(models.Model):
    TYPE_EVENTS = [
        ('CO', 'Соревнования'),
        ('UT', 'УТС'),
        ('EX', 'Экзамен'),
        ('NN', 'Не выбрано'),
    ]

    STATUS_EVENTS = [
        ('NN', 'Отсутствует'),
        ('DC', 'Районные'),
        ('RG', 'Региональные'),
        ('AR', 'Всероссийские'),
        ('IN', 'Международные'),
        ('CE', 'Первенство Европы'),
        ('CR', 'Первенство России'),
        ('CJ', 'Первенство Японии'),
        ('CW', 'Первенство Мира'),
    ]

    name_events = models.CharField(max_length=120, verbose_name='Название мероприятия')
    date_start_events = models.DateField(verbose_name='Дата начала мероприятия')
    date_end_events = models.DateField(verbose_name='Дата окончания мероприятия')
    date_end_applications = models.DateField(verbose_name='Дата окончания подачи заявок')
    limit_ku = models.CharField(max_length=1, blank=True, verbose_name='Ограничение по Кю')
    limit_age = models.CharField(max_length=2, blank=True, verbose_name='Ограничение по возрасту')
    type_events = models.CharField(choices=TYPE_EVENTS,
                                   default='NN',
                                   verbose_name='Тип мероприятия',
                                   max_length=12,
                                   )
    status_events = models.CharField(choices=STATUS_EVENTS,
                                     default='NN',
                                     verbose_name='Статус мероприятия',
                                     max_length=17,
                                     )
    def __str__(self):
        return self.name_events

    def get_absolut_url(self):
        return f'/events/{self.pk}/'


class DocumentsEvents(models.Model):
    name_event = models.ForeignKey(Events,
                                   on_delete=models.CASCADE,
                                   verbose_name='Название мероприятия',
                                   )
    name_doc = models.CharField(max_length=15, verbose_name='Название документа')
    date_doc = models.DateField(verbose_name='Дата документа')
    series_doc = models.CharField(max_length=15, blank=True, verbose_name='Номер документа')
    scan_doc = models.FileField(verbose_name='Файл', blank=True, upload_to='events/%Y-%m-%d')

class ResultEvents(models.Model):
    RESULT = [
        ('1P', '1 место'),
        ('2P', '2 место'),
        ('3P', '3 место'),
        ('4P', '4 место'),
        ('V', 'Воля к победе'),
        ('BT', 'Лучшая техника'),
        ('NN', 'Не выбрано'),
    ]

    name_event = models.ForeignKey(Events,
                                   on_delete=models.PROTECT,
                                   blank=True, null=True,
                                   verbose_name='Название мероприятия',
                                   related_name='name_event')
    sportsmen = models.ForeignKey(Sportsmen,
                                  on_delete=models.PROTECT,
                                  verbose_name='Спортсмен',
                                  related_name='results')
    result = models.CharField(choices=RESULT,
                              default='NN',
                              verbose_name='Результат',
                              max_length=10)
    # def __str__(self):
    #     return self.name_event

class ApplicationsForEvents(models.Model):
    name_event = models.ForeignKey(Events,
                                   on_delete=models.CASCADE,
                                   blank=True, null=True,
                                   verbose_name='Название мероприятия',
                                   related_name='applications')

    sportsmen = models.ForeignKey(Sportsmen,
                                  on_delete=models.CASCADE,
                                  verbose_name='Спортсмен',
                                  )



