from django import forms
from django.utils import timezone
from pip._internal.cli.cmdoptions import list_exclude

from .models import Sportsmen, SportsmenDate, CoachDate, TrainersDocuments, DocumentsSportsmen, Events, DocumentsEvents, \
    ResultEvents, SportsHall, TrainingSchedule, ApplicationsForEvents


class AddSportsmen(forms.ModelForm):
    class Meta:
        model = Sportsmen
        fields = '__all__'

class AddSportsmenData(forms.ModelForm):
    class Meta:
        model = SportsmenDate
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['sportsmen'].widget = forms.HiddenInput()

class AddCoachData(forms.ModelForm):
    class Meta:
        model = CoachDate
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['coach'].widget = forms.HiddenInput()

class AddTrainersDocuments(forms.ModelForm):
    # date_added = forms.DateField(required=False)
    class Meta:
        model = TrainersDocuments
        # fields = ['name_document', 'date_issue', 'validity_period', 'number_document', 'issued_by_whom', 'scanned_copy']
        fields = '__all__'
        # exclude = ['coach']
    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['coach'].widget = forms.HiddenInput()
        # self.fields['date_added'].required = False
        # self.fields['date_added'].widget = forms.HiddenInput()

class AddSportsmenDocuments(forms.ModelForm):
    class Meta:
        model = DocumentsSportsmen
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['sportsmen'].widget = forms.HiddenInput()


class AddEvent(forms.ModelForm):
    class Meta:
        model = Events
        fields = '__all__'

class AddEventDocuments(forms.ModelForm):
    class Meta:
        model = DocumentsEvents
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['name_event'].widget = forms.HiddenInput()


class AddResultsEvent(forms.ModelForm):
    class Meta:
        model = ResultEvents
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['name_event'].widget = forms.HiddenInput()

class AddSportsHall(forms.ModelForm):
    class Meta:
        model = SportsHall
        fields = '__all__'

class AddSchedule(forms.ModelForm):
    class Meta:
        model = TrainingSchedule
        fields = '__all__'

class AddApplications(forms.ModelForm):
    class Meta:
        model = ApplicationsForEvents
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        forms.ModelForm.__init__(self, *args, **kwargs)
        self.fields['name_event'].widget = forms.HiddenInput()