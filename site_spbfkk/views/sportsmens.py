from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView

from site_spbfkk.forms import AddSportsmen, AddSportsmenData, AddSportsmenDocuments
from site_spbfkk.models import Sportsmen, SportsmenDate, DocumentsSportsmen
from site_spbfkk.views.logic import export_to_xls


class SportsmenAll(ListView):
    """Отображение всех спортсменов списком на главной странице"""
    model = Sportsmen
    template_name = 'site_spbfkk/home.html'
    context_object_name = 'sportsmen'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return Sportsmen.objects.all()


def add_sportsmen(request):
    """Функция добавления спортсмена в базу"""
    if request.method == 'POST':
        form = AddSportsmen(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:home')
    else:
        form = AddSportsmen()
    return render(request, 'site_spbfkk/addsportsmen.html', {'form': form,
                                                     'title': 'Добавление спортсмена'})

@permission_required('site_spbfkk.view_trainingschedule')
def add_sportsmen_data(request, pk):
    """Функция лобавления данных спортсмена"""
    sportsmen = Sportsmen.objects.get(pk=pk)
    if request.method == 'POST':
        form = AddSportsmenData(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:home')
    else:
        form = AddSportsmenData(initial={'sportsmen': sportsmen})
    return render(request, 'site_spbfkk/add_sportsmen_data.html', {'form': form,
                                                                    'title': 'Добавление данных спортсмена',
                                                                   'sportsmen':sportsmen})

def edit_sportsmen_data(request, pk):
    """Функция редактирования данных спортсмена"""
    sportsmen_data = get_object_or_404(SportsmenDate, pk=pk)
    if request.method == 'POST':
        form = AddSportsmenData(request.POST, instance=sportsmen_data)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:home')
    else:
        form = AddSportsmenData(instance=sportsmen_data)
    return render(request,
                  'site_spbfkk/add_sportsmen_data.html',
                  {'form': form,
                   'title': 'Изменение данных спортсмена',
                   'sportsmen_data': sportsmen_data}
                  )

class ShowDataSportsmen(ListView):
    """Класс для отображения данных спортсмена"""
    model = SportsmenDate
    template_name = 'site_spbfkk/show_data_sportsmen.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sportsmen'] = Sportsmen.objects.get(pk=self.kwargs['pk'])
        context['documents'] = DocumentsSportsmen.objects.filter(sportsmen__pk=self.kwargs['pk'])
        return context

    def get_queryset(self):
        return SportsmenDate.objects.filter(sportsmen__pk=self.kwargs['pk'])

def delete_sportsmen(request, pk):
    """Функция удаления спортсменов"""
    sportsmen = Sportsmen.objects.get(pk=pk)
    sportsmen.delete()
    return redirect('spbfkk:home')

def sportsmen_documents(request, pk):
    """ Функция добавления документов спортсмена """
    sportsmen = Sportsmen.objects.get(pk=pk)
    if request.method == 'POST':
        form = AddSportsmenDocuments(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:home')
    else:
        form = AddSportsmenDocuments(initial={'sportsmen': sportsmen})
    return render(request, 'site_spbfkk/add_sportsmen_documents.html', {'form': form,
                                                             'title': 'Добавление документа',
                                                                'sportsmen': sportsmen})

def delete_sportsmen_documents(request, pk):
     """ Функция удаления документа спортсмена """
     documents = DocumentsSportsmen.objects.get(pk=pk)
     documents.delete()
     return redirect('spbfkk:home')

def edit_sportsmen_document(request, pk):
    """ Функция для редактирования документа спортсмена """
    sportsmen_document = get_object_or_404(DocumentsSportsmen, pk=pk)
    if request.method == 'POST':
        form = AddSportsmenDocuments(request.POST, request.FILES, instance=sportsmen_document)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:home')
    else:
        form = AddSportsmenDocuments(instance=sportsmen_document)
    return render(request, 'site_spbfkk/add_sportsmen_documents.html', {'form': form,
                                                             'title': 'Изменение документа ',
                                                            'sportsmen_document': sportsmen_document
                                                                        }
                  )

class SportsmenAllRating(ListView):
    """Отображение рейтинга спортсмена"""
    model = SportsmenDate
    template_name = 'site_spbfkk/show_sportsmens_rating.html'
    context_object_name = 'sprt_data'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
         return SportsmenDate.objects.order_by('weight_category_male','weight_category_female','-sports_rating')

def export_excel(request):
    wrapper = export_to_xls()
    filename = f'sportsmens.xlsx'
    response = HttpResponse(
    wrapper.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        )
    response['Content-Disposition'] = f'attachment; filename={filename}'
    return response


