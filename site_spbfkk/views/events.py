from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic import ListView

from site_spbfkk.forms import AddEvent, AddEventDocuments, AddResultsEvent, AddApplications
from site_spbfkk.models import Events, DocumentsEvents, ResultEvents, SportsmenDate, ApplicationsForEvents
from site_spbfkk.views.logic import count_rating


def add_event(request):
    """Функция добавления мероприятия в базу"""
    if request.method == 'POST':
        form = AddEvent(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:all_events')
    else:
        form = AddEvent()
    return render(request, 'site_spbfkk/add_event.html', {'form': form,
                                                             'title': 'Добавление мероприятия'})


class AllEvents(ListView):
    """Отображение всех мероприятий списком на главной странице"""
    model = Events
    template_name = 'site_spbfkk/events.html'
    context_object_name = 'events'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return Events.objects.all()

class ShowEvent(ListView):
    """ Класс для отображения данных мероприятия """
    model = Events
    template_name = 'site_spbfkk/show_event.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['event'] = Events.objects.get(pk=self.kwargs['pk'])
        context['documents'] = DocumentsEvents.objects.filter(name_event__pk=self.kwargs['pk'])
        return context

    def get_queryset(self):
        return Events.objects.filter(pk=self.kwargs['pk'])

def edit_event_data(request, pk):
    """ Функция для редактирования данных мероприятия """
    event_data = get_object_or_404(Events, pk=pk)
    if request.method == 'POST':
        form = AddEvent(request.POST, instance=event_data)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:all_events')
    else:
        form = AddEvent(instance=event_data)
    return render(request, 'site_spbfkk/add_event.html', {'form': form,
                                                            'title': 'Изменение данных мероприятия',
                                                            'event_data': event_data})

def add_event_documents(request, pk):
    """ Функция добавления документов мероприятия"""
    event = Events.objects.get(pk=pk)
    if request.method == 'POST':
        form = AddEventDocuments(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:all_events')
    else:
        form = AddEventDocuments(initial={'name_event': event})
    return render(request, 'site_spbfkk/add_event_documents.html', {'form': form,
                                                             'title': 'Добавление документа',
                                                            'event': event})


def delete_event_documents(request, pk):
    """ Функция удаления документа мероприятия """
    documents = DocumentsEvents.objects.get(pk=pk)
    documents.delete()
    return redirect('spbfkk:all_events')


def edit_event_document(request, pk):
    """ Функция для редактирования документа мероприятия """
    event_document = get_object_or_404(DocumentsEvents, pk=pk)
    if request.method == 'POST':
        form = AddEventDocuments(request.POST, request.FILES, instance=event_document)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:all_events')
    else:
        form = AddEventDocuments(instance=event_document)
    return render(request, 'site_spbfkk/add_event_documents.html', {'form': form,
                                                                        'title': 'Изменение документа ',
                                                                        'event_document': event_document
                                                                        }
                  )

def add_event_results(request, pk):
    """Функция добавления результатов мероприятия в базу"""
    event = Events.objects.get(pk=pk)
    status = event.status_events
    results = ResultEvents.objects.filter(name_event=pk)
    type_event = event.type_events
    if request.method == 'POST':
        form = AddResultsEvent(request.POST)
        if form.is_valid():
            event_result: ResultEvents = form.save()
            if type_event == 'CO' and event_result.result not in ('BT', 'V', 'NN'):
                sportsmen = event_result.sportsmen
                data: SportsmenDate = sportsmen.sportsmensdata.first()
                data.sports_rating = count_rating(data.sports_rating, status, event_result.result)
                data.save()
            return redirect('spbfkk:add_event_results', pk=pk)
    else:
        form = AddResultsEvent(initial={'name_event': event})
    return render(request, 'site_spbfkk/add_event_results.html', {'form': form,
                                                          'title': 'Добавление результатов мероприятия',
                                                            'event': event,
                                                            'result': results,
                                                            })

def delete_event_result(request, pk):
    """ Функция удаления результата """
    result = ResultEvents.objects.get(pk=pk)
    event_pk = result.name_event.pk
    result.delete()
    return redirect('spbfkk:add_event_results', pk=event_pk)
    # return redirect('spbfkk:all_events')

def add_event(request):
    """Функция добавления мероприятия в базу"""
    if request.method == 'POST':
        form = AddEvent(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:all_events')
    else:
        form = AddEvent()
    return render(request, 'site_spbfkk/add_event.html', {'form': form,
                                                             'title': 'Добавление мероприятия'})

def add_event_applications(request, pk):
    """Функция добавления заявок на мероприятие"""

    event = Events.objects.get(pk=pk)
    applications = ApplicationsForEvents.objects.filter(name_event=pk)

    if request.method == 'POST':
        form = AddApplications(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:add_event_applications', pk=pk)
    else:
        form = AddApplications(initial={'name_event': event})
    return render(request, 'site_spbfkk/add_event_applications.html', {'form': form,
                                                          'title': 'Добавление заявок',
                                                            'event': event,
                                                            'applications': applications,
                                                                        })

def delete_event_applications(request, pk):
    """ Функция удаления заявки """
    applct = ApplicationsForEvents.objects.get(pk=pk)
    event_pk = applct.name_event.pk
    applct.delete()
    return redirect('spbfkk:add_event_applications', pk=event_pk)