from .coaches import (
    CoachesAll,
    ShowDataCoach,
    add_coach_data,
    edit_coach_data,
    trainers_documents,
    delete_trainers_documents,
    edit_coach_document,
    CoachesRatings,
)

from .sportsmens import (
    SportsmenAll,
    add_sportsmen,
    add_sportsmen_data,
    edit_sportsmen_data,
    ShowDataSportsmen,
    delete_sportsmen,
    sportsmen_documents,
    delete_sportsmen_documents,
    edit_sportsmen_document,
    export_excel,
    SportsmenAllRating,
)

from .events import (add_event,
                     AllEvents,
                    ShowEvent,
                    edit_event_data,
                    add_event_documents,
                    delete_event_documents,
                    edit_event_document,
                    add_event_results,
                    delete_event_result,
                    add_event_applications,
                    delete_event_applications,
                     )

from .clubs import (
                    SportsHallAll,
                    add_sports_hall,
                    ShowSportsHall,
                    add_schedule,
                    )

from .logic import (logout_user,
                    )