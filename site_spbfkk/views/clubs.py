from django.shortcuts import redirect, render
from django.views.generic import ListView

from site_spbfkk.forms import AddSportsHall, AddSchedule
from site_spbfkk.models import SportsClub, SportsHall, Coach, TrainingSchedule


class SportsHallAll(ListView):
    """Класс для отображения списка спортивных клубов"""
    model = SportsHall
    template_name = 'site_spbfkk/sports_hall_all.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['sportshall'] = SportsHall.objects.get(pk=self.kwargs['pk'])
        return context

def add_sports_hall(request):
    """ Функция для добавления спортивного зала """
    # sports_hall = SportsHall.objects.get(pk=pk)
    if request.method == 'POST':
        form = AddSportsHall(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:coaches')
    else:
        form = AddSportsHall()
    return render(request, 'site_spbfkk/add_sports_hall.html', {'form': form,
                                                             'title': 'Добавление спортивного зала',
                                                              })

class ShowSportsHall(ListView):
    """ Класс для отображения расписания спортивного зала """
    model = TrainingSchedule
    template_name = 'site_spbfkk/show_schedule.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sportshall'] = SportsHall.objects.get(pk=self.kwargs['pk'])
        # context['schedule'] = TrainingSchedule.objects.filter(adress_hall__pk=self.kwargs['pk'])
        # context['clubs'] = SportsClub.objects.get(pk=self.kwargs['pk'])
        # context['documents'] = TrainersDocuments.objects.filter(coach__pk=self.kwargs['pk'])
        return context


    def get_queryset(self):
        return TrainingSchedule.objects.filter(address_hall_id=self.kwargs['pk'])

def add_schedule(request, pk):
    """ Функция для добавления расписания"""
    # sports_hall = SportsHall.objects.get(pk=pk)
    if request.method == 'POST':
        form = AddSchedule(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:coaches')
    else:
        form = AddSchedule()
    return render(request, 'site_spbfkk/add_schedule.html', {'form': form,
                                                             'title': 'Добавление расписания',
                                                              })
