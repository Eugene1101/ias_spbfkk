from io import BytesIO

from django.contrib.auth import logout
from django.shortcuts import redirect
from xlsxwriter import Workbook

from site_spbfkk.models import Sportsmen


def logout_user(request):
    logout(request)
    return redirect('login')

def count_rating(rating, status, result):
    """Функция подсчёта рейтинга спортсмена"""

    value = {
             'DC': {'1P': 15, '2P': 10, '3P': 5, '4P': 0},
             'RG': {'1P': 30, '2P': 20, '3P': 10, '4P': 0},
             'AR': {'1P': 40, '2P': 30, '3P': 20, '4P': 0},
             'IN': {'1P': 40, '2P': 30, '3P': 20, '4P': 0},
             'CE': {'1P': 70, '2P': 60, '3P': 50, '4P': 40},
             'CR': {'1P': 100, '2P': 80, '3P': 60, '4P': 50},
             'CJ': {'1P': 110, '2P': 100, '3P': 80, '4P': 70},
             'CW': {'1P': 200, '2P': 170, '3P': 120, '4P': 100},
             }

    rating_value = value[status][result]
    new_rating = rating + rating_value
    return new_rating


def export_to_xls():
    output = BytesIO()
    wb = Workbook(output, {'in_memory': True})
    sheet = wb.add_worksheet()

    for i, sportsmen in enumerate(Sportsmen.objects.all()):
        sheet.write(i, 0, sportsmen.first_name)
        sheet.write(i, 1, sportsmen.last_name)

    wb.close()
    output.seek(0)
    return output



