import os
from datetime import date

from django.db.models import Subquery, Count, OuterRef, Q
from django.db.models.functions import Coalesce
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404, resolve_url
from django.views.generic import ListView

from ias_spbfkk import settings
from site_spbfkk.forms import AddCoachData, AddTrainersDocuments
from site_spbfkk.models import Coach, CoachDate, TrainersDocuments, Events


class CoachesAll(ListView):
    """ Класс для отображения списка тренеров """
    model = Coach
    template_name = 'site_spbfkk/coaches.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):# можно вызвать атрибутом класса
        return Coach.objects.all()


class ShowDataCoach(ListView):
    """ Класс для отображения данных тренера """
    model = CoachDate
    template_name = 'site_spbfkk/show_data_coach.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['coach'] = Coach.objects.get(pk=self.kwargs['pk'])
        context['documents'] = TrainersDocuments.objects.filter(coach__pk=self.kwargs['pk'])
        return context

    def get_queryset(self):
        return CoachDate.objects.filter(coach__pk=self.kwargs['pk'])

def add_coach_data(request, pk):
    """ Функция для добавления данных тренера """
    coach = Coach.objects.get(pk=pk)
    if request.method == 'POST':
        form = AddCoachData(request.POST)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:coaches')
    else:
        form = AddCoachData(initial={'coach': coach})
    return render(request, 'site_spbfkk/add_coach_data.html', {'form': form,
                                                             'title': 'Добавление данных тренера',
                                                               'coach': coach})

def edit_coach_data(request, pk):
    """ Функция для редактирования данных тренера """
    coach_data = get_object_or_404(CoachDate, pk=pk)
    if request.method == 'POST':
        form = AddCoachData(request.POST, instance=coach_data)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:coaches')
    else:
        form = AddCoachData(instance=coach_data)
    return render(request, 'site_spbfkk/add_coach_data.html', {'form': form,
                                                            'title': 'Изменение данных тренера',
                                                            'coach_data': coach_data})

def trainers_documents(request, pk):
    """ Функция добавления документов тренера """
    coach = Coach.objects.get(pk=pk)
    if request.method == 'POST':
        form = AddTrainersDocuments(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:coaches')
    else:
        form = AddTrainersDocuments(initial={'coach': coach})
    return render(request, 'site_spbfkk/add_trainers_documents.html', {'form': form,
                                                             'title': 'Добавление документа',
                                                                'coach': coach})
def delete_trainers_documents(request, pk):
    """ Функция удаления документа тренера """
    documents = TrainersDocuments.objects.get(pk=pk)
    documents.delete()
    return redirect('spbfkk:coaches') # возможность редиректа на страницу show_coach

# def name_directory_path(instance, filename):
#     return '{0}/{1}'.format(instance.Coach, filename)

def edit_coach_document(request, pk):
    """ Функция для редактирования документа тренера """
    coach_document = get_object_or_404(TrainersDocuments, pk=pk)
    if request.method == 'POST':
        form = AddTrainersDocuments(request.POST, request.FILES, instance=coach_document)
        if form.is_valid():
            form.save()
            return redirect('spbfkk:coaches')
    else:
        form = AddTrainersDocuments(instance=coach_document)
    return render(request, 'site_spbfkk/add_trainers_documents.html', {'form': form,
                                                             'title': 'Изменение документа тренера',
                                                             'coach_document': coach_document})


class CoachesRatings(ListView):
    """ Класс для отображения списка тренеров """
    model = Coach
    template_name = 'site_spbfkk/coaches_rating.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):# можно вызвать атрибутом класса

        d1 = date(2021, 9, 1)
        d2 = date(2022, 8, 31)
        # subquery = Subquery(
            # Events.objects.filter(applications__sportsmen__coach=OuterRef('id'), date_start_events__range=[d1, d2]).annotate(c=Count('id')).values_list('c', flat=True))
        return Coach.objects.annotate(count=Count('sportsmen__applicationsforevents__name_event'),
                                      filter=Q(sportsmen__applicationsforevents__name_event__date_start_events__range=[d1,d2])).distinct()
