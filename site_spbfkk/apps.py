from django.apps import AppConfig


class SiteSpbfkkConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'site_spbfkk'
    verbose_name = 'СПбФКК'
